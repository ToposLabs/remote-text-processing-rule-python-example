import os
import json

'''
SERVICE_NAME = Service name is used as the application ID towards Eureka
EUREKA_SERVICE_URL= The Eureka service endpoint used for registration
EUREKA_SERVICE_PATH = The path of eureka service end point. Default to eureka/apps
EUREKA_INSTANCE_HOSTNAME = The hostname used for registration on eureka.
EUREKA_INSTANCE_PORT = The port number used for the instance
EUREKA_HEARTBEAT = Number of seconds used for updating registration status towards Eureka. Default is 90 seconds
EUREKA_METADATA = Additional metadata for Eureka client to send to server
EUREKA_USE_DNS = <true/false> User hostnames rather than IP addresses 
'''

if 'SERVICE_NAME' not in os.environ:
    SERVICE_NAME = 'remote-text-processing-rule-python-example'
else:
    SERVICE_NAME = os.environ['SERVICE_NAME']

if 'EUREKA_INSTANCE_PORT' not in os.environ:
    os.environ['EUREKA_INSTANCE_PORT'] = '8102'
EUREKA_INSTANCE_PORT = os.environ['EUREKA_INSTANCE_PORT']

if 'EUREKA_URL' not in os.environ:
    os.environ['EUREKA_URL'] = 'http://127.0.0.1'
if os.environ['EUREKA_URL'].endswith('/'):
    os.environ['EUREKA_URL'][:-1]
os.environ['EUREKA_SERVICE_URL'] = os.environ['EUREKA_URL'] + ':' + EUREKA_INSTANCE_PORT
EUREKA_SERVICE_URL = os.environ['EUREKA_URL'] + ':' + EUREKA_INSTANCE_PORT

if 'EUREKA_SERVICE_PATH' not in os.environ:
    os.environ['EUREKA_SERVICE_PATH'] = 'eureka/apps'
EUREKA_SERVICE_PATH = os.environ['EUREKA_SERVICE_PATH']

if 'EUREKA_INSTANCE_HOSTNAME' not in os.environ:
    os.environ['EUREKA_INSTANCE_HOSTNAME'] = '127.0.0.1'
EUREKA_INSTANCE_HOSTNAME = os.environ['EUREKA_INSTANCE_HOSTNAME']

if 'EUREKA_HEARTBEAT' not in os.environ:
    os.environ['EUREKA_HEARTBEAT'] = '60'
EUREKA_HEARTBEAT = os.environ['EUREKA_HEARTBEAT']

EUREKA_USE_DNS = False
if 'EUREKA_USE_DNS' in os.environ and os.environ['EUREKA_USE_DNS'].lower() == 'true':
    EUREKA_USE_DNS = True

if 'EUREKA_METADATA' not in os.environ:
    os.environ['EUREKA_METADATA'] = json.dumps({"type": "text-processing-rule", "management.port": 5000})
EUREKA_METADATA = eval(os.environ['EUREKA_METADATA'])
