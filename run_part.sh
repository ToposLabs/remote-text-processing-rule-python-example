#!/usr/bin/env bash

while [ 1 ];
do
    echo "Run command: $@"
    $@
    echo "Warning: $1 $2 exited - Restarting"
    sleep 3 # sleep 3 seconds
done