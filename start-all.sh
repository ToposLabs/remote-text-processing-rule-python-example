#!/usr/bin/env bash

rule_name="remote_text_processing_rule_python_example"
rule_dir="/opt/${rule_name}"
log_dir="/var/log/edison_rules"
tokenize_words_log_file="${log_dir}/${rule_name}.log"

mkdir -p ${log_dir}
touch ${tokenize_words_log_file}

cd ${rule_dir}
./run_part.sh python3 TestRemoteRuleApp.py  >> ${tokenize_words_log_file} 2>&1 &

tail -f ${tokenize_words_log_file}
