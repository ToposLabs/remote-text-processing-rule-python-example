# use https://app.quicktype.io/#l=schema to help generate schema

TestRemoteConfigSchema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "Test Remote Config",
    "type": "object",
    "additionalProperties": False,
    "properties": {
        "addTextSize": {
            "type": "boolean",
            "default": False,
            "description": "Need to add string size or not",
            "title": "Add string size"
        }
    },
    "required": ["addTextSize"]
}
