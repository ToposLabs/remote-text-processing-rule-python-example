FROM ubuntu:18.04

# ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8

RUN apt-get -y update --fix-missing --no-install-recommends && \
    apt-get install -y locales python-setuptools python python3-pip wget vim net-tools
RUN pip3 install requests flask flask-cors flasgger jsonobject jsonschema dnspython rstr
RUN locale-gen en_US.UTF-8
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*
RUN rm -rf /tmp/* /var/tmp/*
RUN mkdir -p /opt/remote_text_processing_rule_python_example

COPY EurekaConfig.py /opt/remote_text_processing_rule_python_example
COPY TestRemoteRuleApp.py /opt/remote_text_processing_rule_python_example
COPY components /opt/remote_text_processing_rule_python_example/components
COPY controllers /opt/remote_text_processing_rule_python_example/controllers
COPY flask_eureka /opt/remote_text_processing_rule_python_example/flask_eureka

COPY run_part.sh /opt/remote_text_processing_rule_python_example/run_part.sh
COPY start-all.sh /opt/start-all.sh

EXPOSE 5000

RUN sed -i 's/\r//' /opt/remote_text_processing_rule_python_example/run_part.sh
RUN chmod 777 /opt/remote_text_processing_rule_python_example/run_part.sh

RUN sed -i 's/\r//' /opt/start-all.sh
RUN chmod 777 /opt/start-all.sh

CMD ["/opt/start-all.sh"]