from jsonobject import *


class RuleInfo(JsonObject):
    name = None
    description = None
    version = None
    author = None
    includeContextFields = []
    dependencies = []
