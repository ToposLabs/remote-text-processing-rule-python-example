from jsonobject import *


class Annotation(JsonObject):
    start = 0
    end = 0
    text = ''
    features = {}
