import json


class ProcessRequest():
    text = ''
    language = ''
    config = {}

    def __init__(self, obj):
        self.__dict__ = json.loads(obj)
