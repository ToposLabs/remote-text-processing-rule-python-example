# remote-text-processing-rule-python-example

This project requires a custom [flask_eurka](https://gitlab.com/ToposLabs/flask_eureka.git) library.

> git submodule add https://gitlab.com/ToposLabs/flask_eureka.git

IMPORTANT
The flask library will use its hostname in the configuration passed to the flask server. The client must be resolvable
from the server using its hostname!  
 
Please use the geoword-eureka-service as the eureka test server. We have provided a docker image that can be installed using this command:
docker pull toposlabs/geoword-eureka-service

Start geoword-eureka-service:
docker-compose -f docker-compose-geoword-eureka-service.yml up -d

The Eureka server's management UI can now be access on this URL:
http://localhost:8102/

=========================================
This app requires a Eureka serer to be available. See the EurekaConfig for default settings or override using environment variables.
SERVICE_NAME = Service name is used as the application ID towards Eureka
EUREKA_SERVICE_URL= The Eureka service endpoint used for registration
EUREKA_SERVICE_PATH = The path of eureka service end point. Default to eureka/apps
EUREKA_INSTANCE_HOSTNAME = The hostname used for registration on eureka.
EUREKA_INSTANCE_PORT = The port number used for the instance
EUREKA_HEARTBEAT = Number of seconds used for updating registration status towards Eureka. Default is 90 seconds
EUREKA_METADATA = Additional metadata for Eureka client to send to server
EUREKA_USE_DNS = <true/false> User hostnames rather than IP addresses 

To run locally, use:
```
docker-compose -f docker-compose.yml up -d

Once the docker container has been started the Swagger UI can be accessed here:
http://127.0.0.1:5000/apidocs/
```

## Example requests
POST `/rule/process`
```
{
  "text": "Hello world",
  "language": "en",
  "config": {
    "addTextSize": true
  }
}

POST `/rule/config/schema`
```
{
  "addTextSize": true
}

components -
    TestRemoteConfig - process data input class
    TestRemoteConfigSchema - schema used by engine to validate a saved config and to build the UI widgets
controllers - contains classes that represent the data
    info
        RuleInfo - class for the rule information
    process
        ProcessRequest - class for the "/rule/process" API input data
        ProcessResponse - class for the "/rule/process" API response data
```
