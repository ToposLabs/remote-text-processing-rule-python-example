import json

import jsonschema
import rstr
from flasgger import Swagger
from flask import Flask, redirect
from flask import Response
from flask import jsonify
from flask import request

import EurekaConfig
from components import TestRemoteConfigSchema
from components.TestRemoteConfig import TestRemoteConfig
from controllers.info.RuleInfo import RuleInfo
from controllers.process.Annotation import Annotation
from controllers.process.AnnotationSet import AnnotationSet
from controllers.process.Features import Features
from controllers.process.ProcessRequest import ProcessRequest
from controllers.process.ProcessResponse import ProcessResponse
from flask_eureka import Eureka

app = Flask(__name__)
Swagger(app)


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


def obj_to_json_response(obj):
    js = json.dumps(obj,
                    sort_keys=True,
                    indent=4,
                    separators=(',', ': '))

    return Response(js, status=200, mimetype='application/json')


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route("/rule/info", methods=['GET'])
def getInfo():
    """
         Return information about this rule
         ---
         tags:
           - main-controller
         consumes:
           - application/json
         produces:
           - application/json;charset=UTF-8
         responses:
           401:
             description: Unauthorized
           403:
             description: Forbidden
           404:
             description: Not Found
           200:
             description: OK
             example: {
                "author": "string",
                "description": "string",
                "name": "string",
                "version": "string",
                "includeContextFields": "list",
                "dependencies": "list"}
         """
    ruleInfo = RuleInfo()
    ruleInfo.name = "Remote Python rule"
    ruleInfo.description = "This remote rule is python example"
    ruleInfo.version = "1.0.0"
    ruleInfo.author = "Topos Labs"
    ruleInfo.includeContextFields.append("all")
    ruleInfo.dependencies.append("TextParser")
    return obj_to_json_response(ruleInfo.to_json())


@app.route("/rule/config/schema", methods=['GET'])
def getConfigSchema():
    """
         Return default rule schema. The schema is used to validate rule parameters and build UI widgets.
         ---
         tags:
           - main-controller
         produces:
           - application/json;charset=UTF-8
         responses:
           401:
             description: Unauthorized
           403:
             description: Forbidden
           404:
             description: Not Found
           200:
             description: OK
             example: {}
         """
    return obj_to_json_response(TestRemoteConfigSchema.TestRemoteConfigSchema)


@app.route("/rule/config/schema", methods=['POST'])
def validateConfig():
    """
        Validate rule schema.
        ---
        tags:
          - main-controller
        consumes:
          - application/json
        produces:
          - application/json;charset=UTF-8
        parameters:
          - in: body
            name: config
            required: true
            description: Process the rule input data
            example: {}
        responses:
          401:
            description: Unauthorized
          403:
            description: Forbidden
          404:
            description: Not Found
          201:
            description: Created
          200:
            description: OK
            schema:
              id: main-controller
              example: true
         """
    try:
        jsonschema.validate(request.json, TestRemoteConfigSchema.TestRemoteConfigSchema)
        return obj_to_json_response('true')
    except jsonschema.exceptions.ValidationError as e:
        return obj_to_json_response('false')
    except:
        raise InvalidUsage('Error while validating config', status_code=400)

    return obj_to_json_response('false')


# This endpoint should return default config
@app.route("/rule/config", methods=['GET'])
def getDefaultConfig():
    """
         Return default config for rule
         ---
         tags:
           - main-controller
         produces:
           - application/json;charset=UTF-8
         responses:
           401:
             description: Unauthorized
           403:
             description: Forbidden
           404:
             description: Not Found
           200:
             description: OK
             example: {"addTextSize": true}
         """
    trc = TestRemoteConfig()
    return obj_to_json_response(trc.to_json())


# This API used for processing text.
@app.route("/rule/process", methods=['POST'])
def process():
    """
        Process text
        ---
        tags:
          - main-controller
        consumes:
          - application/json
        produces:
          - application/json;charset=UTF-8
        parameters:
          - in: body
            name: processRequest
            required: true
            description: Process the rule input data
            example: {"config": {}, "language": "string", "text": "string"}
        responses:
          401:
            description: Unauthorized
          403:
            description: Forbidden
          404:
            description: Not Found
          201:
            description: Created
          200:
            description: OK
            schema:
              id: main-controller
              example: {"annotationSets": [{"annotations": [{"end": 0,"features": [{"name": "string","value": {}}],"start": 0,"text": "string"}],"name": "string"}]}
              properties:
                text:
                  type: string
                  description: Process text
                  default: Hello world
                language:
                  type: string
                  description: Language of the text
                  default: en
                config:
                  type: object
                  description: Rule params
                  default: { addTextSize": true }


        """
    processRequest = ProcessRequest(json.dumps(request.json))
    testRemoteConfig = json.loads(json.dumps(processRequest.config), object_hook=TestRemoteConfig)

    finalPattern = []
    for c in processRequest.text:
        if c.isalpha():
            if c.isupper():
                finalPattern.append("[A-Z]{1}")
            else:
                finalPattern.append("[a-z]{1}")
        elif c.isdigit():
            finalPattern.append("\\d")
        elif c.isspace():
            finalPattern.append(" ")
        else:
            finalPattern.append("\\")
            finalPattern.append(c)

    processResponse = ProcessResponse()

    annotationSet = AnnotationSet()
    annotationSet.textRandomString = []

    annotation = Annotation()
    annotation.start = 0
    annotation.end = len(processRequest.text)
    annotation.text = processRequest.text

    features = Features()
    features.generatedString_keyword = rstr.xeger(''.join(finalPattern))

    if testRemoteConfig.addTextSize:
        features.textSize_integer = len(processRequest.text)

    annotation.features = features.to_json()
    annotationSet.textRandomString.append(annotation.to_json())

    processResponse.annotationSet.update(annotationSet.to_json())

    return obj_to_json_response(processResponse.to_json())


@app.route('/healthcheck')
def healthcheck():
    return getInfo()


@app.route('/')
def default():
    return redirect("/apidocs/", code=302)


if __name__ == '__main__':
    eureka = Eureka(app)
    eureka.register_service(name=EurekaConfig.SERVICE_NAME, use_dns=EurekaConfig.EUREKA_USE_DNS)
    app.run(debug=True,
            host='0.0.0.0',
            port=5000)
